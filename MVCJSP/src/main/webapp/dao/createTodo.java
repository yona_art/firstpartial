package main.webapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import main.webapp.model.DBConnection;

/**
 * createTodo
 */
public class CreateTodo {

    public int createTodoTaks(String list, int done) {
        try (Connection db = DBConnection.getInstance().getConnection()) {
            PreparedStatement ps = db.prepareStatement("INSERT INTO TO_DO_LIST (list, is_done) values (?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, list);
            ps.setInt(2, done);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return  rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

}