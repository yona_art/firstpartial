package main.webapp.model;

/**
 * TodoBean
 */
public class TodoBean {

    private int id;
    private String list;
    private int is_done;

    public TodoBean(int id, String list, int done) {
        this.id = id;
        this.list = list;
        this.is_done = done;
    }

    public int getId() {
        return id;
    }

    public int getIs_done() {
        return is_done;
    }

    public void setIs_done(int is_done) {
        this.is_done = is_done;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public void setId(int id) {
        this.id = id;
    }

}