package main.webapp.model;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DBConnection
 */
public class DBConnection {

    public static Connection conn;
    public static DBConnection instance;
    private static String URL = "jdbc:mysql://localhost:3306/test?" +
    "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static String user = "root";
    private static String pass = "1234";


    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(URL, user, pass);
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static DBConnection getInstance() {
        if (instance == null) {
            return new DBConnection();
        } else {
            return instance;
        }
    }
}